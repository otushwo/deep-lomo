
## _SSOT at k8s_

#### Что необходимо для развертывания

0.1. GitLab с исходниками микросервисов, которые успешно собираются и выкладываются в хранилище.
- В нашем случае это GitLab Container Registry.

0.2. Хранилище для Helm Chart-ов.
- В нашем случае это GitLab Package Registry.

#### Как всё развернуть
1. Создать k8s кластер. Например, в Google командой:
> ~/bin/google-cloud-sdk/bin/gcloud beta container --project "otus-deeplo" \
> clusters create "mvp" \
> --logging=NONE \
> --zone "europe-central2-a" \
> --machine-type "n1-standard-2" \
> --image-type "COS_CONTAINERD" \
> --disk-type "pd-standard" \
> --disk-size "100" \
> --enable-ip-alias \
> --max-pods-per-node "110" \
> --num-nodes "3" \
> --enable-autoupgrade --enable-autorepair \
> --max-surge-upgrade 1 --max-unavailable-upgrade 0 --node-locations "europe-central2-a"

2. В созданный кластер установить Flux2 с привязкой к проекту в Git-е:
> flux bootstrap git \
> --components-extra=image-reflector-controller,image-automation-controller \
> --url=https://gitlab.com/otushwo/deep-lomo \
> --username=USERNAME \
> --password=PASSWORD \
> --token-auth=true \
> --branch=main \
> --path=infrastructure

2.1 Добавить в кластер для Flux-а дополнительный secret. Необходим для обновления Flux-ом манифестов HelmReleas-ов, в случаях обновления версий контейнеров, развёрнутых в кластере:
> kubectl create secret docker-registry regcred -n flux-system \
> --docker-server=registry.gitlab.com/otushwo/deep-lomo/ \
> --docker-username=USERNAME \
> --docker-password=PASSWORD \
> --docker-email=mick.jugger@rolling-stones.the

3. В соответсвующий проект (и конечно branch) Git-а, добавить содержимое папки "infrastructure"

4. Подождать 4-6 минут, пока всё развернётся.

##### _готово_
В случае, если в хранилище контенеров появится новая версия, Flux самостоятельно изменит файл (в Git) соответсвующего HelmReleas-а, после чего HelmRelease развёрнутый в кластере обновится.

-----------------
В случае необходимости внесения изменений, например сменить FQDN для ingress-сервиса Grafana, меняем в Git соответсвующий файл (infrastructure/releases/monitoring/grafana-ingress.yaml)

Все микросервисы представлены как отдльный HelmRelease.
При этом, GitLab выступает как HelmRepository.
Так как Helm Chart сам по себе меняется редко, в данном случае они созданы вручную.
Например для микросервиса frontend процесс создания чарта и добавление его в helm-репозитарий таков:
1. создать пустой чарт
> helm create frontend
2. внести изменения, как вам требуется
3. запаковать его
> helm package ./frontend
4. добавить репозитарий локально
> helm repo add --username USER --password PASS hipster-shop https://gitlab.com/api/v4/projects/35905066/packages/helm/stable
(много цифр в середине url-а, это ID проекта в GitLab)
5. залить пакет в репозитарий
> helm cm-push ./frontend-0.1.0.tgz hipster-shop

##### Описание содержимого папки infrastructure

flux-system - создаётся в момент установки Flux в кластер;

imageupdate - здесь манифесты, описывающие процесс слежения за обновлением имиджей и что с этим делать;

namespaces - no comments;

releases - манифесты HelmRelease;

repositories - манифесты HelmRepository;

workouts - всё, что не вошло в другие директории.
